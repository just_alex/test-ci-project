FROM node:16.19.1-alpine3.17

COPY package.json /app/package.json
COPY package-lock.json /app/package-lock.json
WORKDIR /app

RUN npm ci --only-production

COPY . /app
RUN npm run build

CMD ["npm", "run", "start:prod"]

# curl http://localhost:3000
